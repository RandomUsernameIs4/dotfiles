(message ".emacs start")

(if (< emacs-major-version 24)
		(progn 
			(add-to-list 'load-path "~/.emacs.d/lisp/before24")
			(add-to-list 'load-path "~/.emacs.d/lisp/ecb")
			(require 'ecb))
	(progn
		(require 'package)
    (add-to-list 'package-archives '("MELPA" . "https://melpa.org/packages/") t)
    ;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
    ;; and `package-pinned-packages`. Most users will not need or want to do this.
    (add-to-list 'package-archives '("MELPA Stable" . "https://stable.melpa.org/packages/") t)
    (add-to-list 'package-archives '("GNU ELPA" . "https://elpa.gnu.org/packages/") t)
		;; (setq package-archives
		;; 	     '(("GNU ELPA" . "https://elpa.gnu.org/packages/")
		;; 	     ("MELPA Stable" . "https://stable.melpa.org/packages/")
		;; 	     ("MELPA" . "https://melpa.org/packages/"))
		;; 	     package-archive-priorities
		;; 	     '(("MELPA Stable" . 10)
		;; 	       ("MELPA" . 5)
		;; 	       ("GNU ELPA" . 0)))
		(package-initialize))
	)

(add-to-list 'load-path "~/.emacs.d/lisp/spinner.el")
(require 'spinner)
(add-to-list 'load-path "~/.emacs.d/lisp/lsp-mode")
(require 'lsp-mode)

(require 'ivy)
;;---------------------------------------------------------------------------------------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#32302F" "#FB4934" "#B8BB26" "#FABD2F" "#83A598" "#D3869B" "#17CCD5" "#EBDBB2"])
 '(bm-cycle-all-buffers t)
 '(bm-electric-show t)
 '(bm-in-lifo-order t)
 '(bm-recenter t)
 '(bookmark-sort-flag 'last-modified)
 '(c-basic-offset 2)
 '(c-default-style '((java-mode . "java") (awk-mode . "awk") (other . "gnu")))
 '(c-offsets-alist '((substatement-open . +) (inlambda . 0)))
 '(cc-search-directories
   '("." "../inc" "../src" "/usr/include" "/usr/local/include/*"))
 '(clean-buffer-list-kill-never-buffer-names
   '("*scratch*" "*Messages*" "*server*" "*compilation*" "*Python*" "*xref*"))
 '(company-backends
   '(company-cmake
     (company-dabbrev-code company-dabbrev company-abbrev company-capf)
     company-files))
 '(company-completion-started-hook nil)
 '(company-dabbrev-downcase nil)
 '(company-echo-truncate-lines nil)
 '(company-frontends '(company-pseudo-tooltip-frontend))
 '(company-idle-delay nil)
 '(company-insertion-on-trigger nil)
 '(company-insertion-triggers nil)
 '(company-rtags-insert-arguments nil)
 '(company-selection-wrap-around t)
 '(company-show-quick-access ''left)
 '(company-tabnine-auto-balance nil)
 '(company-tabnine-log-file-path "/tmp/tabnine.log")
 '(company-tabnine-max-num-results 20)
 '(company-tooltip-idle-delay 1)
 '(company-tooltip-margin 20)
 '(company-tooltip-maximum-width 100)
 '(company-tooltip-minimum 1)
 '(company-transformers '(company-sort-by-occurrence))
 '(compilation-ask-about-save nil)
 '(compilation-scroll-output 'first-error)
 '(compilation-skip-threshold 2)
 '(compilation-window-height 7)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes
   '("790f741ffa83f2d75897a0cd44f2aa625ff801f54b367fc00f55679e1769a176" "98a619757483dc6614c266107ab6b19d315f93267e535ec89b7af3d62fb83cad" "fc0c179ce77997ecb6a7833310587131f319006ef2f630c5a1fec1a9307bff45" "62408b3adcd05f887b6357e5bd9221652984a389e9b015f87bbc596aba62ba48" default))
 '(desktop-auto-save-timeout 0)
 '(desktop-base-lock-name "")
 '(desktop-clear-preserve-buffers
   '("\\*scratch\\*" "\\*Messages\\*" "\\*server\\*" "\\*tramp/.+\\*" "\\*Warnings\\*" "\\*Compilation\\*"))
 '(desktop-path '("~/.emacs.d/" "~/.emacs.d/desktops/" "~"))
 '(desktop-save nil)
 '(desktop-save-mode t)
 '(directory-abbrev-alist nil)
 '(doc-view-continuous t)
 '(ecb-auto-activate t)
 '(ecb-compilation-buffer-names
   '(("*Calculator*")
     ("*vc*")
     ("*vc-diff*")
     ("*Apropos*")
     ("*Occur*")
     ("*shell*")
     ("\\*[cC]ompilation.*\\*" . t)
     ("\\*i?grep.*\\*" . t)
     ("*JDEE Compile Server*")
     ("*Help*")
     ("*Completions*")
     ("*Backtrace*")
     ("*Compile-log*")
     ("*bsh*")
     ("*xref*")
     ("*Messages*" . t)))
 '(ecb-compile-window-height 10)
 '(ecb-compile-window-prevent-shrink-below-height t)
 '(ecb-compile-window-temporally-enlarge nil)
 '(ecb-history-make-buckets 'extension)
 '(ecb-layout-name "methods-history-speedbar")
 '(ecb-layout-window-sizes
   '(("left16"
      ("ecb-history-buffer-name" 0.2 . 0.75)
      (ecb-directories-buffer-name 0.2 . 0.25))))
 '(ecb-options-version "2.50")
 '(ecb-tip-of-the-day nil)
 '(ecb-tree-indent 2)
 '(ede-auto-add-method 'multi-ask)
 '(ediff-make-buffers-readonly-at-startup t)
 '(ediff-merge-split-window-function 'split-window-vertically)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(egg-git-diff-options '("--patience" "--ignore-space-change"))
 '(etags-select-go-if-unambiguous t)
 '(etags-table-search-up-depth 5)
 '(flymake-proc-master-file-dirs '("~/.flake8" "." "./src" "./UnitTest"))
 '(gdb-many-windows nil)
 '(gdb-same-frame nil)
 '(gdb-show-main t)
 '(ggtags-auto-jump-to-match nil)
 '(ggtags-mode-prefix-key "\3")
 '(ggtags-sort-by-nearness t)
 '(global-auto-complete-mode nil)
 '(global-auto-revert-mode t)
 '(global-company-mode t)
 '(global-hl-line-mode t)
 '(global-semantic-decoration-mode t)
 '(global-semantic-highlight-func-mode t)
 '(global-semantic-idle-local-symbol-highlight-mode t nil (semantic/idle))
 '(global-semantic-idle-summary-mode t)
 '(global-semantic-stickyfunc-mode t)
 '(global-visual-line-mode t)
 '(gtags-auto-update t)
 '(gtags-grep-all-text-files t)
 '(gtags-path-style 'relative)
 '(gud-gdb-command-name "gdb -i=mi")
 '(gud-tooltip-mode t)
 '(highlight-indent-guides-auto-even-face-perc 10)
 '(highlight-indent-guides-auto-odd-face-perc 0)
 '(highlight-indent-guides-auto-stack-character-face-perc 90)
 '(highlight-indent-guides-auto-top-character-face-perc 70)
 '(highlight-indent-guides-method 'fill)
 '(highlight-indent-guides-responsive 'top)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(initial-frame-alist '((fullscreen . maximized)))
 '(ivy-mode t)
 '(kill-whole-line t)
 '(magit-blame-styles
   '((headings
      (heading-format . "%H %-20a %C %s\12"))
     (margin
      (margin-format "%-4H" " %s%f" " %C %a" " %H")
      (margin-width . 42)
      (margin-face . magit-blame-margin)
      (margin-body-face magit-blame-dimmed))
     (highlight
      (highlight-face . magit-blame-highlight))
     (lines
      (show-lines . t)
      (show-message . t))))
 '(markdown-command "pandoc")
 '(markdown-command-needs-filename t)
 '(markdown-enable-highlighting-syntax t)
 '(markdown-hide-markup t)
 '(midnight-mode t nil (midnight))
 '(package-selected-packages
   '(ac-helm flyspell-correct-helm kubernetes-helm wconf mustache k8s-mode company-terraform terraform-doc terraform-mode edbi bind-key 0blayout boxy tile window-layout elpy elpygen pygen magit-delta magit-tbdiff eshell-vterm vterm-toggle vterm starhugger tabnine company-tabnine ac-math auctex-latexmk auctex-lua cdlatex company-math latex-change-env lsp-latex magic-latex-buffer math-tex-convert org-edit-latex org-elp org-fragtog org-latex-impatient px texfrag xenops auto-yasnippet autobookmarks aws-snippets awscli-capf ivy-yasnippet py-snippets yasnippet-classic-snippets yasnippet-snippets yasnippet latex-extra latex-pretty-symbols latex-preview-pane latex-table-wizard latexdiff auctex auto-complete-auctex citar company-auctex evil-tex laas jack ob-browser org-preview-html flymd gh-md grip-mode markdown-mode markdown-preview-mode markdownfmt cfn-mode emr yaml yaml-mode highlight-indentation hl-indent hl-indent-scope vdiff jenkinsfile-mode agtags counsel-gtags ggtags helm-gtags company-ctags counsel-etags cmake-mode ccls docker docker-api docker-cli docker-compose-mode dockerfile-mode groovy-mode company company-irony company-irony-c-headers company-shell irony butler jenkins jenkins-watch swiper switch-buffer-functions ibuffer-projectile projectile projectile-codesearch projectile-git-autofetch projectile-variable treemacs-projectile magit-gitflow magit-stgit magit-svn magit-topgit magit-vcsh treemacs treemacs-magit highlight-doxygen flycheck-rtags rtags magit egg eshell-git-prompt git-blamed git-command git-messenger git-timemachine gitconfig-mode ivy ivy-rtags company-rtags diff-hl highlight-indent-guides hl-todo flycheck-color-mode-line flycheck-status-emoji flycheck-tip flymake-python-pyflakes flymake-sass bm etags-select etags-table company-c-headers ac-etags ctags-update flycheck-clang-tidy flycheck-clangcheck ## sass-mode todotxt irony-eldoc fuzzy-match fuzzy-format fuzzy ergoemacs-status ergoemacs-mode ecb darktooth-theme ctags cpputils-cmake cmake-project cmake-ide cmake-font-lock auto-complete-exuberant-ctags auto-complete-clang auto-complete-c-headers async ac-clang))
 '(pdf-latex-command "lualatex")
 '(pop-up-windows t)
 '(popwin-mode nil)
 '(popwin:adjust-other-windows t)
 '(popwin:special-display-config
   '(("*Miniedit Help*" :noselect t)
     (help-mode)
     (completion-list-mode :noselect t)
     (compilation-mode :noselect t)
     (grep-mode :noselect t)
     (occur-mode :noselect t)
     ("*Pp Macroexpand Output*" :noselect t)
     ("*Shell Command Output*")
     ("*vc-diff*")
     ("*vc-change-log*")
     (" *undo-tree*" :width 60 :position right)
     ("^\\*anything.*\\*$" :regexp t)
     ("*slime-apropos*")
     ("*slime-macroexpansion*")
     ("*slime-description*")
     ("*slime-compilation*" :noselect t)
     ("*slime-xref*")
     (sldb-mode :stick t)
     (slime-repl-mode)
     (slime-connection-list-mode)))
 '(pos-tip-background-color "#36473A")
 '(pos-tip-foreground-color "#FFFFC8")
 '(python-shell-completion-native-enable nil)
 '(rtags-bury-buffer-function 'quit-window)
 '(rtags-close-taglist-on-focus-lost t)
 '(rtags-completions-enabled t)
 '(rtags-display-result-backend 'default)
 '(rtags-find-file-case-insensitive t)
 '(rtags-jump-to-first-match nil)
 '(rtags-path "/usr/bin")
 '(rtags-popup-results-buffer t)
 '(rtags-show-containing-function t)
 '(rtags-tracking t)
 '(same-window-buffer-names
   '("*shell*" "*mail*" "*inferior-lisp*" "*ielm*" "*scheme*" "*Help*" "*Completions*" "*xref*"))
 '(semantic-default-submodes
   '(global-semantic-highlight-func-mode global-semantic-decoration-mode global-semantic-stickyfunc-mode global-semantic-idle-scheduler-mode global-semanticdb-minor-mode global-semantic-idle-summary-mode global-semantic-idle-local-symbol-highlight-mode global-semantic-show-unmatched-syntax-mode global-semantic-show-parser-state-mode))
 '(semantic-stickyfunc-indent-string "")
 '(sh-basic-offset 2)
 '(sh-indent-after-do nil)
 '(sh-indent-after-loop-construct nil)
 '(sh-indent-for-case-alt '+)
 '(sh-indent-for-continuation nil)
 '(sh-indentation 2)
 '(show-paren-mode t)
 '(speedbar-default-position 'right)
 '(speedbar-sort-tags t)
 '(standard-indent 2)
 '(tab-stop-list '(1 2 8 16 24 32 40 48 56 64 72 80 88 96 104 112 120))
 '(tab-width 2)
 '(tags-revert-without-query t)
 '(temporary-bookmark-p nil)
 '(wg-morph-on nil)
 '(wg-prefix-key "\25")
 '(wg-query-for-save-on-emacs-exit nil)
 '(wg-query-for-save-on-workgroups-mode-exit nil)
 '(workgroups-mode t)
 '(yaml-mode-hook
   '(yaml-set-imenu-generic-expression highlight-indent-guides-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Noto Mono" :foundry "GOOG" :slant normal :weight normal :height 98 :width normal))))
 '(bm-face ((t nil)))
 '(bm-fringe-face ((t nil)))
 '(bm-fringe-persistent-face ((t (:background "dark green" :foreground "White"))))
 '(bm-persistent-face ((t (:background "DarkGreen" :foreground "White"))))
 '(hl-line ((t (:extend t :background "#400040"))))
 '(region ((t (:background "SlateBlue3"))))
 '(semantic-highlight-func-current-tag-face ((t (:background "gray30")))))

;;---------------------------------------------------------------------------------------------------

(add-to-list 'load-path "~/.emacs.d/lisp")

(add-to-list 'company-frontends 'company-tng-frontend)

(if (file-exists-p "~/.emacs.d/lisp/system-includes.el")
		(load-file "~/.emacs.d/lisp/system-includes.el"))

(if (< emacs-major-version 24)
		(progn
			(add-to-list 'load-path "~/.emacs.d/lisp/cl-lib/")
			(require 'cl-lib)
			(require 'auto-complete)
			(require 'auto-complete-config)))

(if (< emacs-major-version 24)
		(progn
			(require 'color-theme)
			(color-theme-initialize)
			(color-theme-charcoal-black))
	(progn
		(require 'darktooth-theme)
		(load-theme 'darktooth t))
	)

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(delete-selection-mode t)

;; (wconf-load "~/.emacs.d/wconf-layouts/0")
;; (wconf-use-next)

(hl-line-mode)

(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

(setq backup-directory-alist
      `((".*" . ,"~/tmp/emacs")))
(setq auto-save-file-name-transforms
      `((".*" ,"~/tmp/emacs" t)))				

(ido-mode t)

(require 'bm)
(defun my/jump-hook()
	(let ((bookmark (bm-bookmark-at (point))))
    (unless bookmark
			(bm-bookmark-add))
		)
	)
(add-hook 'ff-pre-find-hook 'my/jump-hook)

(defun my/c-hook()
	(global-set-key (kbd "<f6>") 'gud-gdb)
	(defun my/jump()
		(interactive)
		(my/jump-hook)
		(rtags-find-symbol-at-point)
		)
	
	(defun doxygen-put()
		(interactive)
		(beginning-of-line)
		(backward-char 1)
		(insert "\n/**")
		(c-indent-command)
		(insert "\n* ")
		(c-indent-command)
		(insert "\n*/")
		(c-indent-command)
		(forward-line -1)
		(end-of-line)
		)

  ;;	(local-set-key (kbd "C-.") 'my/jump)
  (local-set-key (kbd "M-S-<left>") 'bm-previous)
  (local-set-key (kbd "M-S-<right>") 'bm-next)
	(local-set-key (kbd "M-S-<up>") 'bm-show-all)
;;	(local-set-key (kbd "C->") 'rtags-find-all-references-at-point)
	(local-set-key (kbd "C-/") 'rtags-display-summary)
	(if (>= emacs-major-version 24)
			(progn
				(cppcm-reload-all)
				))
	)
(add-hook 'c++-mode-hook 'my/c-hook)
(add-hook 'c-mode-hook 'my/c-hook)

;;https://stackoverflow.com/questions/23553881/emacs-indenting-of-c11-lambda-functions-cc-mode
(defun vr-c++-looking-at-lambda_as_param ()
  "Return t if text after point matches '[...](' or '[...]{'"
  (looking-at ".*[,(][ \t]*\\[[^]]*\\][ \t]*[({][^}]*?[ \t]*[({][^}]*?$"))

(defun vr-c++-looking-at-lambda_in_uniform_init ()
  "Return t if text after point matches '{[...](' or '{[...]{'"
  (looking-at ".*{[ \t]*\\[[^]]*\\][ \t]*[({][^}]*?[ \t]*[({][^}]*?$"))

(defun vr-c++-indentation-examine (langelem looking-at-p)
  (and (equal major-mode 'c++-mode)
       (ignore-errors
         (save-excursion
           (goto-char (c-langelem-pos langelem))
           (funcall looking-at-p)))))

(defun vr-c++-indentation-setup ()
  (require 'google-c-style)
  (google-set-c-style)

  (c-set-offset
   'block-close
   (lambda (langelem)
     (if (vr-c++-indentation-examine
          langelem
          #'vr-c++-looking-at-lambda_in_uniform_init)
         '-
       0)))

  (c-set-offset
   'statement-block-intro
   (lambda (langelem)
     (if (vr-c++-indentation-examine
          langelem
          #'vr-c++-looking-at-lambda_in_uniform_init)
         0
       '+)))

  (defadvice c-lineup-arglist (around my activate)
    "Improve indentation of continued C++11 lambda function opened as argument."
    (setq ad-return-value
          (if (vr-c++-indentation-examine
               langelem
               #'vr-c++-looking-at-lambda_as_param)
              0
            ad-do-it))))
;;--------------------------------------------------------------------------------------------
(defun my/latex-hook()
	(local-set-key (kbd "<backtab>") 'tex-insert-macro)
	)
(add-hook 'latex-mode-hook 'my/latex-hook)
;;-------------------------------------------------------------------------------------------


(global-ede-mode 1)

(add-hook 'comint-mode-hook
					(lambda()
						(local-set-key (kbd "<up>") 'comint-previous-input)
						(local-set-key (kbd "<down>") 'comint-next-input)
						)
					)

(defun my/programming-hook()
  )

(defun my/python-hook()
  (my/programming-hook)
	(setq py-smart-indentation t)
	(local-set-key (kbd "<backtab>") 'company-complete)
  (elpy-enable)
  (eval-after-load "elpy"
    '(cl-dolist (key '("M-<up>" "M-<down>" "M-<left>" "M-<right>"))
       (define-key elpy-mode-map (kbd key) nil)))
  (local-set-key (kbd "C->") 'xref-find-references)
	)
(add-hook 'python-mode-hook 'my/python-hook)

(defun my/django-mode()
	(python-mode t)

  (add-to-list 'load-path "~/.emacs.d/lisp/django-mode/")
  (require 'django-html-mode)
  (require 'django-mode)
  (add-to-list 'auto-mode-alist '("\\.djhtml$" . django-html-mode))
  )

;;https://www.emacswiki.org/emacs/download/joseph-single-dired.el
(add-to-list 'load-path "~/.emacs.d/lisp/joseph-single-dired/")
(require 'joseph-single-dired)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;;---------------------------------------------------------------------------------------------------
(defun my/func/sql-mark-and-send()
  (interactive)
  (if mark-active
      (progn
        (sql-send-region (region-beginning) (region-end))
        )
    (progn
      (sql-send-region (line-beginning-position 1) (line-beginning-position 2))
      )
    )
  )

(defun my/hooks/sql-keymap()
  (local-set-key (kbd "<f5>") `my/func/sql-mark-and-send)
  (local-set-key (kbd "C-<f5>") `sql-send-buffer)
  (message "SQL hook")
	)
(add-hook 'sql-mode-hook 'my/hooks/sql-keymap)

;;--------------------------------------------------------------------------------------------------

(global-set-key (kbd "C-<tab>") 'buffer-menu)
(global-set-key (kbd "M-<left>") 'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<up>") 'windmove-up)
(global-set-key (kbd "M-<down>") 'windmove-down)
(global-set-key (kbd "C-a") 'mark-whole-buffer)
(global-set-key (kbd "M-S-<down>") 'bm-toggle)
(global-set-key (kbd "M-S-<left>") 'bm-previous)
(global-set-key (kbd "M-S-<right>") 'bm-next)
(global-set-key (kbd "M-S-<up>") 'bm-show-all)
(global-set-key (kbd "<backtab>") 'company-complete)
(global-set-key (kbd "C-<iso-lefttab>") 'yas-expand-from-trigger-key)
(global-set-key (kbd "C-e") 'ecb-goto-window-history)
(global-set-key (kbd "C-S-e") 'ecb-goto-window-directories)
(global-set-key (kbd "<S-f2>") 'treemacs)
(global-set-key (kbd "<f2>") 'treemacs)
(global-set-key (kbd "C-x <up>") 'ff-find-other-file)
(global-set-key (kbd "C-x C-g") 'magit-find-file)
(global-set-key (kbd "<S-delete>") 'kill-whole-line)
(global-set-key (kbd "<f5>") 'compile)
(global-set-key (kbd "C-.") 'isearch-forward-symbol-at-point)
(global-set-key (kbd "C->") 'highlight-symbol-at-point)
(global-set-key (kbd "C-0") 'forward-sexp)
(global-set-key (kbd "C-9") 'backward-sexp)
(define-key ivy-minibuffer-map (kbd "M-RET") 'ivy-immediate-done)

(define-key company-active-map (kbd "TAB") 'company-select-next)
(define-key company-active-map (kbd "<backtab>") 'company-select-previous)
(define-key help-mode-map (kbd "<tab>") 'company-select-next)
(define-key help-mode-map (kbd "<backtab>") 'company-select-previous)
(define-key company-active-map (kbd "RET") nil)
(eval-after-load 'yasnippet
  `(progn
    (define-key yas-minor-mode-map [(tab)]        nil)
    (define-key yas-minor-mode-map (kbd "TAB")    nil)
    (define-key yas-minor-mode-map (kbd "<tab>")  nil)))

;; terminator
(define-key input-decode-map "\e[2~" [insert])
(define-key input-decode-map "\e[3~" [delete])
(define-key input-decode-map "\e[0H" [home])
(define-key input-decode-map "\e[0F" [end])
(define-key input-decode-map "\e[5~" [prior])
(define-key input-decode-map "\e[6~" [next])
(define-key input-decode-map "\e[3~" [?\C-d])
(define-key input-decode-map "\e[3;2~" [S-delete])
(define-key input-decode-map "\e[1;2A" [S-up])
(define-key input-decode-map "\e[1;2B" [S-down])
(define-key input-decode-map "\e[1;2C" [S-right])
(define-key input-decode-map "\e[1;2D" [S-left])
(define-key input-decode-map "\e[1;3A" [M-up])
(define-key input-decode-map "\e[1;3B" [M-down])
(define-key input-decode-map "\e[1;3C" [M-right])
(define-key input-decode-map "\e[1;3D" [M-left])

(message ".emacs ok!")
(put 'scroll-left 'disabled nil)

python
import os, sys
sys.path.append(os.environ['HOME']+'/.gdb')
import mycommands
end

source ~/.gdb/stl-views

set follow-fork-mode parent
set detach-on-fork on
set history save on
set history size unlimited
set history remove-duplicates unlimited

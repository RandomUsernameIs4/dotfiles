#!/bin/bash

if (( $# < 1 )); then
  echo "usage: $0 <new volume setting>
example: $0 +5%">&2
  exit 1
fi

DEFAULT_SINK_NAME="RUNNING"
INDEX=`pactl list short sinks|grep "$DEFAULT_SINK_NAME"|tail -n 1|sed 's/^\([[:digit:]]\+\)[[:space:]].*/\1/'`

if BLUEZ=`pactl list short sinks|grep bluez`; then
  INDEX=`echo $BLUEZ|cut -d ' ' -f1`
fi

pactl set-sink-volume "$INDEX" "$1"

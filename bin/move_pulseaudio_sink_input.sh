#!/bin/bash

if (( $# < 1 )); then
  echo "usage: $0 <pulseaudio client application.process.binary> <pulseaudio sink name>
example: $0 firefox alsa_output.pci-0000_00_1f.3.analog-stereo">&2
  exit 1
fi



SINK_NAME="$2"
SINK_INDEX=`pactl list short sinks|grep "$SINK_NAME"|tail -n 1|sed 's/^\([[:digit:]]\+\)[[:space:]].*/\1/'`

PROCESS_BINARY="$1"
for INPUT_INDEX in `pactl list sink-inputs |grep "Sink Input #\|application.process.binary"|grep -B1 "application.process.binary = \"firefox\""|grep "Sink Input"|sed 's/[^#]\+#\([[:digit:]]\+\)/\1/'`; do
  pactl move-sink-input $INPUT_INDEX $SINK_INDEX
done
